'use client'

import { MDBCard, MDBCardBody, MDBCardFooter, MDBCardHeader, MDBCardImage, MDBCardText, MDBCardTitle, MDBCol, MDBContainer, MDBIcon, MDBRow } from 'mdb-react-ui-kit';
import Image from "next/image";

export default function Home() {
  return (
    <>
       <MDBContainer fluid >
        <MDBRow center>
          <MDBCol className='gx-5 m-5' center >
              <h5 className='text-center' style={{ color:"#5E35B1",fontWeight:'bold'}}>GoPharma</h5>
              <h1 className='text-center' style={{ color:"#5E35B1",fontWeight:'bold'}}>EL COMERCIO DIGITAL</h1>
              <h5 className='text-center'>El Comercio Digital en Venezuela ha experimentado un crecimiento y transformaciónes
              significativas en los últimos años, la adopción de tecnologías móviles y el acceso a internet están impulsando ese crecimiento resiliente, impulsado por la adaptación a desafíos únicos y una población cada vez más conectada. La innovación local son factores claves para el futuro del comercio y es por ello desde GoPharma hemos diseñado y creado una nueva manera de comercializar productos e insumos farmacéuticos en el mercado Venezolano.</h5>

            
          </MDBCol>
        </MDBRow>
        <MDBRow className='row-cols-1 row-cols-md-3 g-4 mx-3 mb-5' >
          <MDBCol style={{  backgroundImage: `url('images/dot-big-square.svg')` }} >
            <MDBCard className='h-100'>
            <MDBCardHeader background='transparent' border='success' className='text-center'>
                <MDBIcon style={{ color: '#f0c41e' }}  icon='rocket' size='3x' />
              </MDBCardHeader>
              <MDBCardBody className='text-center'>
                <MDBCardTitle style={{ color: '#5E35B1',fontWeight:'bold' }}>Misión</MDBCardTitle>
                <MDBCardText>
                El Enfoque en la intersección de la tecnología y la medicina nos condujo al desarrolllo de una Aplicación Movil innovadora.
                </MDBCardText>
              </MDBCardBody>
            </MDBCard>
          </MDBCol>
          <MDBCol style={{  backgroundImage: `url('images/dot-big-square.svg')` }} >
            <MDBCard className='h-100'>
              <MDBCardHeader background='transparent' border='success' className='text-center'>
                <MDBIcon style={{ color: '#5E35B1' }} icon='eye' size='3x' />
              </MDBCardHeader>
              <MDBCardBody className='text-center'>
                <MDBCardTitle style={{ color: '#5E35B1',fontWeight:'bold' }}>Visión</MDBCardTitle>
                <MDBCardText>
                Ser una empresa lider con la finalidad de desarrollar productos innovadores para asi tener una alta participación en el mercado.
                </MDBCardText>
              </MDBCardBody>
            </MDBCard>
          </MDBCol>
          <MDBCol style={{  backgroundImage: `url('images/dot-big-square.svg')` }} >
            <MDBCard className='h-100'>
            <MDBCardHeader background='transparent' border='success' className='text-center'>
                <MDBIcon fas icon="lightbulb"  size='3x' style={{ color: '#29933e' }}/>
              </MDBCardHeader>
              <MDBCardBody className='text-center'>
                <MDBCardTitle style={{ color: '#5E35B1',fontWeight:'bold' }}>Objetivos</MDBCardTitle>
                <MDBCardText>
                Simplificar la gestión de ventas a domicilio de medicamentos y productos relacionados, para la satisfacción de las necesidades de las pequeñas y grandes farmacias.
                </MDBCardText>
              </MDBCardBody>
            </MDBCard>
          </MDBCol>
          
        </MDBRow>
       </MDBContainer>
    </>
  );
}
