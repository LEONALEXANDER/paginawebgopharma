'use client'
import React, { useState, useEffect } from 'react';
import Image from "next/image";
import {
    MDBNavbar,
    MDBContainer,
    MDBNavbarBrand,
    MDBIcon,
    MDBNavbarNav,
    MDBNavbarItem,
    MDBNavbarLink,
    MDBNavbarToggler,
    MDBCollapse,
    MDBRow,
    MDBCol,
    MDBCarousel,
    MDBCarouselItem,
    MDBCarouselCaption
} from 'mdb-react-ui-kit';

function Header() {
    const [openNavText, setOpenNavText] = useState(false);
    const [scrolled, setScrolled] = useState(false);
    const [textColor, setTextColor] = useState('text-light');

    useEffect(() => {
        const handleScroll = () => {
            if (window.scrollY > 400) { // Ajusta la posición del scroll según tus necesidades
                setScrolled(true);
                setTextColor('text-dark')
            } else {
                setScrolled(false);
                setTextColor('text-light')
            }
        };

        window.addEventListener('scroll', handleScroll);

        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, []);

    return (
        <>
        <style>
        {`
         
      
          /* Height for devices larger than 576px */
          @media (min-width: 992px) {
            #introCarousel {
              margin-top: -108.59px;
            }

            #introCarousel,
            .carousel-inner,
            .carousel-item,
            .carousel-item.active {
              height: 50vh;
            }
          }
        `}
      </style>
            <MDBNavbar expand='lg' style={{ backgroundColor: scrolled ? '#ffffff' : 'rgba(255,255,255,0.0)' ,zIndex: 2000 }} className='navbar navbar-expand-lg navbar-dark d-none d-lg-block sticky-top'>
                <MDBContainer fluid>
                    <MDBNavbarBrand href='#'>
                        <Image
                            src="/images/logo-nuevo.png"
                            width={100}
                            height={75}
                            alt="Error"
                        />
                    </MDBNavbarBrand>
                    <MDBNavbarToggler
                        type='button'
                        data-target='#navbarText'
                        aria-controls='navbarText'
                        aria-expanded='false'
                        aria-label='Toggle navigation'
                        onClick={() => setOpenNavText(!openNavText)}
                    >
                        <MDBIcon icon='bars' fas />
                    </MDBNavbarToggler>
                    <MDBCollapse navbar open={openNavText} id="navbarExample01" >
                        <MDBNavbarNav right fullWidth={false} className='mr-auto mb-2 mb-lg-0 '>
                            <MDBNavbarItem className='nav-item'>
                                <MDBNavbarLink active aria-current='page' href='#' className={textColor} >
                                    Conócenos
                                </MDBNavbarLink>
                            </MDBNavbarItem>
                            <MDBNavbarItem className='nav-item'>
                                <MDBNavbarLink href='#' className={textColor}>Servicios</MDBNavbarLink>
                            </MDBNavbarItem>
                            <MDBNavbarItem className='nav-item'>
                                <MDBNavbarLink href='#' className={textColor}>Contáctanos</MDBNavbarLink>
                            </MDBNavbarItem>
                        </MDBNavbarNav>
                    </MDBCollapse>
                </MDBContainer>
            </MDBNavbar>
           
            <MDBCarousel showIndicators showControls fade id="introCarousel" className='carousel'>
                <MDBCarouselItem itemId={1}>
                    <img src='https://mdbootstrap.com/img/Photos/Slides/img%20(22).jpg' className='d-block w-100' alt='...' />
                    <MDBCarouselCaption>
                    <p  className='d-none d-lg-block mb-3' style={{color: "#FFB116" }}>EL SOFTWARE N° 1 EN VENEZUELA</p> 
                        <h2 className='mb-1'>GoPharma es una empresa enfocada en crear una red de Farmacias Independientes en Venezuela.</h2> 
                        <h6 className=' d-none d-lg-block' style={{color: "#ffffffb3" }}>La gestión de medicamentos nunca había sido tan sencilla y segura. Descarga nuestra aplicación móvil y comienza a disfrutar de los beneficios de comprar tus medicamentos desde la comodidad de tu hogar, ahorrando tiempo y con la tranquilidad de estar adquiriendo productos de calidad.</h6>
                    </MDBCarouselCaption>
                </MDBCarouselItem>

                <MDBCarouselItem itemId={2} >
                    <img src='https://mdbootstrap.com/img/Photos/Slides/img%20(15).jpg' className='d-block w-100' alt='...' />
                    <MDBCarouselCaption>
                    <h5>Second slide label</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </MDBCarouselCaption>
                </MDBCarouselItem>

                <MDBCarouselItem itemId={3}>
                    <img src='https://mdbootstrap.com/img/Photos/Slides/img%20(23).jpg' className='d-block w-100' alt='...' />
                    <MDBCarouselCaption>
                    <h5>Third slide label</h5>
                    <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                    </MDBCarouselCaption>
                </MDBCarouselItem>
            </MDBCarousel>
        </>
    )
}

export default Header;